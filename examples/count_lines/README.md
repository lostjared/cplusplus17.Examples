
Example output from Program:

	jared@LostSideDead[~/Source/gitlab/acidcam-cli]$ count_lines
	Source File Found: ./acidcam/acidcam-cli-main.cpp
	Source File Found: ./acidcam/acidcam-cli.cpp
	Source File Found: ./acidcam/acidcam-cli.hpp
	Source File Found: ./plugins/blank/source.cpp
	Source File Found: ./plugins/example/source.cpp
	Source File Found: ./plugins/vertsort/source.cpp
	./acidcam/acidcam-cli-main.cpp: 405 lines 35 blank lines for total of 440
	./acidcam/acidcam-cli.cpp: 307 lines 45 blank lines for total of 352
	./acidcam/acidcam-cli.hpp: 100 lines 20 blank lines for total of 120
	./plugins/blank/source.cpp: 5 lines 3 blank lines for total of 8
	./plugins/example/source.cpp: 16 lines 3 blank lines for total of 19
	./plugins/vertsort/source.cpp: 42 lines 3 blank lines for total of 45
	6 file(s) contain: 875 lines and 109 blank lines for a total of 984.
